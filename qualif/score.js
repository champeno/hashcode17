function best_latency_diff(solution, endpoint, video) {
    for (var i = 0 ; i < endpoint.sorted_caches_latencies.length ; i++) {
        let s = endpoint.sorted_caches_latencies[i];
        if (solution.caches[s].cached_videos.includes(video)) {
            return endpoint.latency_from_datacenter - endpoint.connected_caches_latencies[s];
        }
    }
    return 0;
}

function total_requests(problem) {
    let sum = 0;
    problem.requests.forEach(r => sum += r.nb_queries);
    return sum;
}

module.exports = function score(solution, problem) {
    // sum on all requests
    let sum = 0;
    problem.requests.forEach(r => sum += r.nb_queries * (best_latency_diff(solution, r.source, r.video)));
    return Math.floor((sum * 1000) / total_requests(problem));
}
