module.exports = class Endpoint {
	constructor (parameters, problem) {
		// Constants
		this.id 						= parameters.id;
		this.latency_from_datacenter    = parameters.latency_from_datacenter;
		this.nb_connected_cache_servers = parameters.nb_connected_cache_servers;

		// List of servers the endpoint is connected to
		// This list is supposedly sorted (shortest latencies first)
        this.connected_caches_latencies = null;
        this.sorted_caches_latencies    = null;

        // Parameters.connected_caches_latencies : Array(couples(id, latence))
		let caches_latencies = parameters.connected_caches_latencies;
        caches_latencies.sort(function(a, b){ return a[1] - b[1]; });
        let caches_latencies_dict = {};
        let caches_latencies_list = [];
        for (let i = 0; i < caches_latencies.length; i++) {
            let [id, latency] = caches_latencies[i];
            caches_latencies_dict[id] = latency;
            caches_latencies_list.push(id);
        }

        this.connected_caches_latencies = caches_latencies_dict;
        this.sorted_caches_latencies = caches_latencies_list;
	}
}
