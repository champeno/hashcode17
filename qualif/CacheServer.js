module.exports = class CacheServer {
	constructor (parameters) {
		// Constants
		this.id 	      = parameters.id;
		this.max_capacity = parameters.max_capacity;

		this.cached_videos      = [];
		this.remaining_capacity = parameters.max_capacity;
		this.full 			    = false;
        this.video_in_cache     = {};
	}

	storeVideo (video) {
		// Check if remaining space is sufficient
		if (video.size > this.remaining_capacity)
			return false;
        if (this.video_in_cache[video.id])
            return true;        

		// If it is, video is added to the list of cached videos
		this.cached_videos.push(video);

        this.video_in_cache[video.id] = true;
		this.remaining_capacity -= video.size;
		if (video.size === this.remaining_capacity)
			this.full = true;

		return true;
	}
}
