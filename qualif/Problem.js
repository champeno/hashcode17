const Endpoint = require("./Endpoint.js");
const CacheServer = require("./CacheServer.js");
const Video = require("./Video.js");
const Request = require("./Request.js");
const FileSystem = require('fs');

module.exports = class Problem {

    constructor (parameters) {
        // Constants of the problem
        this.nb_videos        = parameters.nb_videos;
        this.nb_endpoints     = parameters.nb_endpoints;
        this.nb_requests      = parameters.nb_requests;
        this.nb_cache_servers = parameters.nb_cache_servers;
        this.server_capacity  = parameters.server_capacity;

        this.endpoints     = parameters.endpoints;
        this.cache_servers = parameters.cache_servers;
        this.videos        = parameters.videos;
        this.requests      = parameters.requests;
    }

    static fromFile (filename) {
        // Read the input file (synchronously)
        let file_content = FileSystem.readFileSync(filename, {
            encoding: "ascii"
        });
        let content_lines = file_content.split('\n');

        // Parameter to build a Problem object
        let parameters = {};

        // Extract the constants and the cells
        let constants = content_lines[0].split(' ');
        parameters.nb_videos        = parseInt(constants[0]);
        parameters.nb_endpoints     = parseInt(constants[1]);
        parameters.nb_requests      = parseInt(constants[2]);
        parameters.nb_cache_servers = parseInt(constants[3]);
        parameters.server_capacity  = parseInt(constants[4]);
            
        // Videos
        parameters.videos = [];

        let video_sizes = content_lines[1].split(' ');
        video_sizes.forEach(function (size, id) {
            parameters.videos[id] = new Video({id: id, size: size});
        });
        
        // Endpoints
        parameters.endpoints = [];

        // For each endpoint...
        let current_line = 1;

        for (let id = 0; id < parameters.nb_endpoints; id++) {
            current_line++;
            let endpoint_parameters = {};
            
            let endpoint_constants = content_lines[current_line].split(' ');
            endpoint_parameters.id                         = id;
            endpoint_parameters.latency_from_datacenter    = parseInt(endpoint_constants[0]);
            endpoint_parameters.nb_connected_cache_servers = parseInt(endpoint_constants[1]);

            // For each cache server the endpoint is connected to...
            endpoint_parameters.connected_caches_latencies = [];
            for (let i = 0; i < endpoint_parameters.nb_connected_cache_servers; i++) {
                current_line++;
                
                let current_connection = content_lines[current_line].split(' ');
                endpoint_parameters.connected_caches_latencies.push([parseInt(current_connection[0]),
                                                                     parseInt(current_connection[1])]);
            }

            parameters.endpoints[id] = new Endpoint(endpoint_parameters);
        }

        // For each request...
        parameters.requests = [];
        
        for (let i = 0; i < parameters.nb_requests; i++) {
            current_line++;
            let request_parameters = {};
            
            let request_constants = content_lines[current_line].split(' ');
            let video_id                  = parseInt(request_constants[0]);
            request_parameters.video      = parameters.videos[video_id];
            let source_id                 = parseInt(request_constants[1]);
            request_parameters.source     = parameters.endpoints[source_id];
            request_parameters.nb_queries = parseInt(request_constants[2]);

            parameters.requests.push(new Request(request_parameters));
        }

        // Create a list of CacheServer instances
        parameters.cache_servers = [];

        for (let i = 0; i < parameters.nb_cache_servers; i++) {
            parameters.cache_servers[i] = new CacheServer({id: i,
                                                           max_capacity: parameters.server_capacity})
        }

        // Finally, a new Problem instance is created
        return new Problem(parameters);
    }

}
