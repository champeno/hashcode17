const FileSystem = require('fs');

class SimpleAssociation {
    constructor (request) {
        this.endpoint = request.source;
        this.nb_requests = request.nb_queries;
        this.request = request;
        this.video = request.video;
        this.score = 0;
    }

    cacheIsFull () {
        return this.best_cache === undefined
            || (this.best_cache !== null && this.best_cache.remaining_capacity < this.video.size);
    }

    rescore (problem) {
        if (!this.cacheIsFull()) return;
        let cache_ids = this.endpoint.sorted_caches_latencies;
        let best_cache = null;
        let best_cache_latency = 0;
        let caches = problem.cache_servers;
        for (let j = 0; j < cache_ids.length; j++) {
            // Assumed : cache_ids are sorted by increasing latency
            let cache_id = cache_ids[j];
            let cache = caches[cache_id];
            if (cache.remaining_capacity >= this.video.size ) {
                best_cache = cache;
                best_cache_latency = this.endpoint.connected_caches_latencies[cache_id];
                break;
            }
        }
        this.best_cache = best_cache;
        if (best_cache === null)
            this.score = Infinity;
        else
            this.score = this.nb_requests * (this.endpoint.latency_from_datacenter / best_cache_latency);
    }

    apply () {
        this.best_cache.storeVideo(this.request.video);
    }

    static compare (a, b) {
        return b.score - a.score;
    }
}

class Solution {
    constructor (cache_servers) {
        this.caches = cache_servers;
    }

    formatOutput () {
        let result = "";
        //console.log(this.caches);
        let caches = this.caches;
        result += caches.length + "\n";
        for (let i = 0; i < caches.length; i++) {
            let cache = caches[i];
            result += cache.id + " ";
            result += cache.cached_videos.map((x) => x.id).join(" ");
            result += "\n";
        }
        this.formatted_output = result;
        return result;
    }

	writeInFile (filename) {
		// If the solution is not formatted yetn formats it
		if (! this.formatted_output_is_available)
			this.formatOutput();

		// Write to the output file (synchronously)
        let file_content = FileSystem.writeFileSync(filename,
        											this.formatted_output);
	}
}

// A simple solution
module.exports = function(problem) {
    let caches = problem.cache_servers;
    let requests = problem.requests;
    let associations = new Array(requests.length);
    
    for (let r = 0; r < requests.length; r++) {
        let request = requests[r];
        associations[r] = new SimpleAssociation(request);
    }
    
    while (associations.length > 0) {
        // If designated cache is full, check if there remains a not-full cache
        if (associations[0].cacheIsFull()) { // true on first iteration
            if (associations[0].score === Infinity) break;
            for (let i = 0; i < associations.length; i++)
                associations[i].rescore(problem);
            associations.sort(SimpleAssociation.compare);
        }
        let association = associations.splice(0, 1)[0];
        //console.log(association);
        if (association.score < Infinity)
            association.apply();
    }

    //console.log(problem.cache_servers);
    return new Solution(problem.cache_servers);
}

