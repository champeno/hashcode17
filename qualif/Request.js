module.exports = class Request {
	constructor (parameters) {
		// Constants
		this.video      = parameters.video;
		this.source     = parameters.source; // endpoint it comes from
		this.nb_queries = parameters.nb_queries;
	}
}