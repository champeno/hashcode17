// -----------------------------------------------------------------------------
// This file aims to handle high-level operations on the problem
// In other words, it loads a problem, solves it, and produces an output
// -----------------------------------------------------------------------------

const Problem = require("./Problem.js");
const solver  = require("./simple.js");
const Path    = require('path');

const computeScore = require("./score.js");


// Global flag to set up/off debug console printing
const PRINT_DEBUG_MESSAGES = true;

// Special function to print debug messages
function printDebuggingMessage (message) {
    if (PRINT_DEBUG_MESSAGES)
        console.log(message);
}

// -----------------------------------------------------------------------------

// COMMAND LINE ARGUMENTS
// First two values are ignored (node program, source file)
const command_line_args = process.argv.slice(2);

// -----------------------------------------------------------------------------

// Get the input path
const input_path = command_line_args[0];
printDebuggingMessage("Input file: " + input_path);

// Makes sure the file is valid and well handled
try {
    let problem = Problem.fromFile(input_path);
    printDebuggingMessage("Problem successfuly created from input file.");
    printDebuggingMessage(problem);

    // Solves the problem, and produces a solution objet
    let solution = solver(problem);
    printDebuggingMessage("Solution objet successfuly computed:");
    // printDebuggingMessage(solution);

    // Score computation
    let score = computeScore(solution, problem);
    console.log("> Computed score: " + score);

    let output_path = "./outputs/" + Path.basename(input_path, ".in") + ".out";
    solution.writeInFile(output_path);

    printDebuggingMessage("Output has been produced.");
}
catch (error) {
    console.log("\n--------- AN ERROR OCCURRED ---------\n")
    console.trace(error);
    process.exit(1);
}
