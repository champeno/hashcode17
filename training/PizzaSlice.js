const Pizza = require("./Pizza.js");

module.exports = class PizzaSlice {

    constructor (r1, r2, c1, c2, pizza) {
        this.r1 = Math.min(r1, r2);
        this.r2 = Math.max(r1, r2);
        this.c1 = Math.min(c1, c2);
        this.c2 = Math.max(c1, c2);
        this.pizza = pizza;
        
        let num_tomatoes = 0;
        let num_mushrooms = 0;
        for (let i = r1; i <= r2; i++) {
            for (let j = c1; j <= c2; j++) {
	            if (pizza.cells[i][j] === Pizza.TOMATO)
                    num_tomatoes++;
                else
                    num_mushrooms++;
            }
        }
        this.num_tomatoes = num_tomatoes;
        this.num_mushrooms = num_mushrooms;
    }

    grow (d_r, d_c, apply=true) {
        let r1, r2, c1, c2;
        if (d_r == -1) [r1, r2, c1, c2] = [this.r1-1, this.r1-1,
                                          this.c1, this.c2];
        else if (d_r == 1) [r1, r2, c1, c2] = [this.r2+1, this.r2+1,
                                              this.c1, this.c2];
        else if (d_c == -1) [r1, r2, c1, c2] = [this.r1, this.r2,
                                               this.c1-1, this.c1-1];
        else if (d_c == 1) [r1, r2, c1, c2] = [this.r1, this.r2,
                                              this.c2+1, this.c2+1];
        let num_tomatoes = this.num_tomatoes;
        let num_mushrooms = this.num_mushrooms;
        for(let i = r1; i <= r2; i++) {
            for(let j = c1; j <= c2; j++) {
                if(! this.pizza.free_cells[i][j]) return false;
	            if(this.pizza.cells[i][j] === Pizza.TOMATO)
                    num_tomatoes++;
                else
                    num_mushrooms++;
            }
        }
        if(num_tomatoes + num_mushrooms <= this.pizza.max_cells_per_slice) {
            if(apply) {
                this.r1 = Math.min(r1, this.r1);
                this.r2 = Math.max(r2, this.r2);
                this.c1 = Math.min(c1, this.c1);
                this.c2 = Math.max(c2, this.c2);
                this.num_tomatoes = num_tomatoes;
                this.num_mushrooms = num_mushrooms;
            }
            return true;
        } else {
            return false;
        }
    }

    is_valid () {
        return this.num_tomatoes + this.num_mushrooms
                 <= this.pizza.max_cells_per_slice;
    }

    is_enough () {
        return this.num_tomatoes >= this.pizza.min_ingredients_per_slice
            && this.num_mushrooms >= this.pizza.min_ingredients_per_slice;
    }

    apply () {
        for(let r = this.r1; r <= this.r2; r++) {
            for(let c = this.c1; c <= this.c2; c++) {
                this.pizza.free_cells[r][c] = false;
            }
        }
    }
}
