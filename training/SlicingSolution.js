// This class represent a solution to the pizza slicing problem
// It is notably able to produce an output file

const FileSystem = require('fs');

module.exports = class SlicingSolution {

	constructor (slices) {
		// A list of pizza slices
		this.slices = slices;

		// Formatted output verion of the solution
		this.formatted_output 			   = "";
		this.formatted_output_is_available = false;
	}

	formatOutput () {
		let formatted_string = "";

		// First line: total number of slices
		formatted_string += this.slices.length;

		// Following lines: r1 c1 r2 c2 of each slice
		this.slices.forEach(function (slice) {
			formatted_string += "\n";

			formatted_string += slice.r1 + " ";
			formatted_string += slice.c1 + " ";
			formatted_string += slice.r2 + " ";
			formatted_string += slice.c2;
		});

		this.formatted_output = formatted_string;
	}

	writeInFile (filename) {
		// If the solution is not formatted yetn formats it
		if (! this.formatted_output_is_available)
			this.formatOutput();

		// Write to the output file (synchronously)
        let file_content = FileSystem.writeFileSync(filename,
        											this.formatted_output);
	}
}