const Pizza           = require("./Pizza.js");
const PizzaSlice      = require("./PizzaSlice.js");
const SlicingSolution = require("./SlicingSolution.js");

const VERTICAL   = 12;
const HORIZONTAL = 25;

shuffle = function shuffle (arr) {
    for (let i = 0; i < arr.length; i++) {
        let p = Math.floor(Math.random() * i);
        [arr[p], arr[i]] = [arr[i], arr[p]];
    }
}

module.exports.minimalSolution = function minimalSolution (pizza) {
    var seeds = [];
    for (let r = 0; r < pizza.nb_rows; r++) {
        for (let c = 0; c < pizza.nb_rows; c++) {
            if (r < pizza.nb_rows-1 
	        &&  pizza.cells[r][c] != pizza.cells[r+1][c])
                seeds.push([r, c, VERTICAL]);

            if (c < pizza.nb_columns-1
            &&  pizza.cells[r][c] != pizza.cells[r][c+1])
                seeds.push([r, c, HORIZONTAL]);
	    }
    }

    function is_free(r, c, mode) {
        return (pizza.free_cells[r][c] &&
                ((mode === VERTICAL && pizza.free_cells[r+1][c]) ||
                 (mode === HORIZONTAL && pizza.free_cells[r][c+1])));
    }

    shuffle(seeds);
    var slices = [];
    
    while (seeds.length > 0) {
        let [r0, c0, direction0] = seeds.pop();
        if (!is_free(r0, c0, direction0)) continue;
        let slice = new PizzaSlice(r0, (direction0 === VERTICAL ? r0+1:r0),
                                   c0, (direction0 === HORIZONTAL ? c0+1:c0),
                                   pizza);
        let deltas = [[1, 0], [-1, 0], [0, 1], [0, -1]];
        shuffle(deltas);
        let i_deltas = 0;
        
        while (!slice.is_enough() && slice.is_valid()) {
            console.log(deltas[0]);
            if (!slice.grow(...deltas[i_deltas]))
                deltas.splice(i_deltas, 1);
            i_deltas++;
            if (i_deltas == deltas.length) {
                shuffle(deltas);
                i_deltas = 0;
            }
        }
        if (slice.is_enough() && slice.is_valid()) {
            slice.apply();
            slices.push(slice);
        }
    }

    return new SlicingSolution(slices);
}
