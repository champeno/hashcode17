const FileSystem = require('fs');

module.exports = class Pizza {

    static get TOMATO   () { return 'T'; }
    static get MUSHROOM () { return 'M'; }

    constructor (nb_rows, nb_columns, min_ingredients_per_slice,
                 max_cells_per_slice, cells) {
        // Constants of the problem
        this.nb_rows                   = nb_rows;
        this.nb_columns                = nb_columns;
        this.min_ingredients_per_slice = min_ingredients_per_slice;
        this.max_cells_per_slice       = max_cells_per_slice;

        // Two-dimensions table containing all the pizza cells
        // By convention, the order is: cells[ROW][COLUMN]
        this.cells = cells;

        // Two-dimensions table indicating if a cell is free or not
        this.free_cells = []

        for (let i = 0; i < nb_rows; i++) {
            this.free_cells[i] = [];
            for (let j = 0; j < nb_columns; j++) {
                this.free_cells[i][j] = true;
            }
        }
    }

    static fromFile (filename) {
        // Read the input file (synchronously)
        let file_content = FileSystem.readFileSync(filename, {
            encoding: "ascii"
        });
        let content_lines = file_content.split('\n');

        // Extract the constants and the cells
        let constants = content_lines[0].split(' ');
        constants.forEach(str => parseInt(str));
        
        let cells          = [];
        let current_string = "";

        // Loop over the lines
        for (let i = 0; i < constants[0]; i++) {
            cells[i]       = [];
            current_string = content_lines[i + 1];

            // Loop over the columns
            for (let j = 0; j < constants[1]; j++) {
                let current_char = current_string.charAt(j);

                cells[i][j] = current_char == 'T'
                            ? Pizza.TOMATO
                            : Pizza.MUSHROOM;
            }
        }

        // Finally, a new Pizza instance is created
        return new Pizza(...constants, cells);
    }

}
